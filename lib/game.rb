require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player
  def initialize(player_one, player_two)
    @board = Board.new
    @player_one = player_one
    @player_one.mark = :X
    @player_two = player_two
    @player_two.mark = :O
    @current_player = @player_one
  end

  def play
    play_turn while @board.over? == false
    @board.display
    if @board.winner.nil?
      puts "It's a draw!"
    else
      puts "Winner is #{@current_player.name}!\n\n"
    end

  end

  def play_turn
    @current_player.display(@board)
    # check place in grid is not empty
    pos = @current_player.get_move
    @board.place_mark(pos, @current_player.mark)
    return true if @board.over?
    switch_players!
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

end


puts 'Welcome to Tic. Tac. Toe.'
puts 'Enter your name, player one:'
name = gets.chomp
game = Game.new(HumanPlayer.new(name), ComputerPlayer.new("Comp"))
game.play
