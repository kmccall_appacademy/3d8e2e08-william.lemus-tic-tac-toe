class Board
  attr_reader :grid
  def initialize(grid = [])
    @grid = grid
    return unless grid.empty?
    @grid = Array.new(3) { Array.new(3) }
  end

  def display
    return if grid.flatten.all?(&:nil?)

    grid.each_with_index do |line, idx|
      puts((line.map do |ch|
        if ch.nil?
          '   '
        else
          " #{ch} "
        end
      end).join('|'))
      puts '___________' unless idx == 2
    end
    puts
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def winner
    win = check_grid(@grid)
    return win unless win.nil?
    win = check_grid(@grid.transpose)
    return win unless win.nil?
    right_diag = [@grid[0][0], @grid[1][1], @grid[2][2]]
    win = check_line(right_diag)
    return win unless win.nil?
    left_diag = [@grid[0][2], @grid[1][1], @grid[2][0]]
    check_line(left_diag)
  end

  def check_grid(temp_grid)
    win = nil
    temp_grid.each do |row|
      win = check_line(row)
      break unless win.nil?
    end
    win
  end

  def check_line(line)
    return :X if line.all? { |mark| mark == :X }
    return :O if line.all? { |mark| mark == :O }
    nil
  end

  def over?
    if winner.nil?
      return false if @grid.flatten.any?(&:nil?)
    end
    true
  end

  def valid_place(pos)
    return true if pos.all? { |el| (0..2).cover?(el) }
    puts "Invalid move, please try again\n"
    false
  end
end
