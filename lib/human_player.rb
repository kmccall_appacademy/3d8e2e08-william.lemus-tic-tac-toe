class HumanPlayer
  attr_reader :name
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def get_move
    puts "#{name}, where would you like to place an #{mark}?"
    valid_pos = nil
    while valid_pos.nil?
      pos = gets.chomp.split(',')
      puts
      valid_pos = check_move(pos)
    end
    valid_pos

  end

  def check_move(pos)
    # input is real
    return pos.map(&:to_i) if pos.length == 2 && numbers?(pos)
    puts 'Invalid input, please try again'
    nil
  end

  def numbers?(pos)
    pos.all? { |el| el.to_i.to_s == el.split[0] && (0..2).cover?(el.to_i) }
  end

  def display(board)
    board.display
  end
end
