class ComputerPlayer
  attr_reader :board, :name
  attr_accessor :mark
  def initialize(name)
    @name = name

  end

  def display(board)
    @board = board
  end

  def get_move
    # find empty places
    empties = empty_places
    empties.each do |pos|
      @board.place_mark(pos, @mark)
      return pos if @board.winner == @mark
      @board.place_mark(pos, nil)
    end
    pos = empties[rand(0...empties.length)]
    @board.place_mark(pos, @mark)
    pos
  end

  def empty_places
    result = []
    (0..2).each do |idx1|
      (0..2).each do |idx2|
        result << [idx1, idx2] if @board.empty?([idx1, idx2])
      end
    end
    result
  end

end
